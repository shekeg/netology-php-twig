<?php
require_once 'vendor/autoload.php';

class Di
{
    static $di = null;
    public static $db;

    public static function get()
    {
        if (! self::$di) {
            self::$di = new Di();
        }
        return self::$di;
    }

    public function config()
    {
        $config = include 'config.php';
        return $config;
    }

    public function db()
    {
        $config = $this->config();
        if (!empty(self::$db)) 
          return self::$db;
        try {
            self::$db = new PDO(
                'mysql:host='.$config['host'].';dbname='.$config['dbname'].';charset=utf8',
                $config['user'],
                $config['pass']
            );
        } catch (PDOException $e) {
            die('Database error: '.$e->getMessage().'<br/>');
        }
        return self::$db;
    }

    public function render($template, $params = [])
    {
      $loader = new Twig_Loader_Filesystem('template');
      $twig = new Twig_Environment($loader, [
        'cache' => 'tmp',
        'auto_reload' => true
      ]);
      echo $twig->render($template,  $params);
    }
}

include 'router/router.php';