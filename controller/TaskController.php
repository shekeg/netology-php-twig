<?php
  include 'model/Task.php';
  include 'model/User.php';
  



  class TaskController {

    private static function getMainList($sortBy = '') {
      $mainList = [];

      $user = new User();
      $mainList['users'] = $user->getList();
      $mainList['currentUser'] = $user->getCurrentUser();
      $login = $mainList['currentUser']['login'];
      $userID = $mainList['currentUser']['user_id'];

      $task = new Task();
      $mainList['tasksForAuthor'] = $task->getListForAuthor($userID, $sortBy);
      $mainList['tasksForAssigned'] = $task->getListForAssigned($userID, $sortBy);

      return $mainList;
    }

    public function getList() {
      User::checkAuth();
      $mainList = self::getMainList();
      Di::get()->render('task.twig', [
        'tasksForAuthor' => $mainList['tasksForAuthor'],
        'currentUser' => $mainList['currentUser'],
        'users' => $mainList['users'],
        'tasksForAssigned' => $mainList['tasksForAssigned']
      ]);
    }

    public function sort() {
      User::checkAuth();
      if (!empty($_POST['sort_by'])) {
        $mainList = self::getMainList($_POST['sort_by']);
        Di::get()->render('task.twig', [
          'tasksForAuthor' => $mainList['tasksForAuthor'],
          'currentUser' => $mainList['currentUser'],
          'users' => $mainList['users'],
          'tasksForAssigned' => $mainList['tasksForAssigned']
        ]);
      }
    }

    public function add() {
      User::checkAuth();
      $mainList = self::getMainList();
      $task = new Task();
      $userID = $mainList['currentUser']['user_id'];
      $errors = [];
      if (count($_POST) > 0) {
        if (!empty($_POST['description'])) {
          $data['description'] = $_POST['description'];
        } else {
          $errors['descriptionBlank'] = 'Добавление не возможно! Описание задачи не может быть пустым.';
        }
        if (!empty($userID)) {
          $data['userID'] = $userID;
        } else {
          $errors['userIDBlank'] = 'Не удалось определить автора задачи';
        }
        if (count($errors) === 0) {
          $isAdd = $task->add($data);
          if ($isAdd) {
            header('Location: ./?c=task&a=getList');
          }
        }
      }
      Di::get()->render('task.twig', [
        'currentUser' => $mainList['currentUser'],
        'tasksForAuthor' => $mainList['tasksForAuthor'],
        'currentUser' => $mainList['currentUser'],
        'users' => $mainList['users'],
        'tasksForAssigned' => $mainList['tasksForAssigned'],
        'addErrors' => $errors
      ]);
    }

    public function editPrepare($params) {
      User::checkAuth();
      if (isset($params['id']) && is_numeric($params['id'])) {
        $mainList = self::getMainList();
        $task = new Task();
        $taskForEdit = $task->find($params['id']);
        if ($taskForEdit) {
          Di::get()->render('task.twig', [
            'currentUser' => $mainList['currentUser'],
            'taskForEdit' => $taskForEdit,
            'tasksForAuthor' => $mainList['tasksForAuthor'],
            'currentUser' => $mainList['currentUser'],
            'users' => $mainList['users'],
            'tasksForAssigned' => $mainList['tasksForAssigned']
          ]);
        }
      }
    }

    public function save($params) {
      User::checkAuth();
      if (isset($params['id']) && is_numeric($params['id'])) {
        $task = new Task();
        $mainList = self::getMainList();
        $errors = [];
        if (count($_POST) > 0) {
          if (!empty($_POST['description'])) {
            $data['description'] = $_POST['description'];
          } else {
            $errors['descriptionBlank'] = 'Сохранение не возможно! Описание задачи не может быть пустым.';
          }
          if (count($errors) === 0) {
            $isSave = $task->save($params['id'], $data);
            if ($isSave) {
              header('Location: ./?c=task&a=getList');
            }
          }
  
        }
        Di::get()->render('task.twig', [
          'currentUser' => $mainList['currentUser'],
          'tasksForAuthor' => $mainList['tasksForAuthor'],
          'currentUser' => $mainList['currentUser'],
          'users' => $mainList['users'],
          'tasksForAssigned' => $mainList['tasksForAssigned'],
          'saveErrors' => $errors
        ]);
      }
    }

    public function complete($params) {
      User::checkAuth();
      if (isset($params['id']) && is_numeric($params['id'])) {
        $task = new Task();
        $isComplete = $task->complete($params['id']);
        if ($isComplete) {
            header('Location: ./?c=task&a=getList');
        }
      }
    }

    public function delete($params) {
      User::checkAuth();
      if (isset($params['id']) && is_numeric($params['id'])) {
        $task = new Task();
        $isDelete = $task->delete($params['id']);
        if ($isDelete) {
            header('Location: ./?c=task&a=getList');
        }
      }
    }

    public function changeAssign($params) {
      User::checkAuth();
      if (isset($params['id']) && is_numeric($params['id'])) {
        $task = new Task();
        $mainList = self::getMainList();
        if (count($_POST) > 0) {
          if (!empty($_POST['assigned_user_id'])) {
            $data['assignedUserID'] = $_POST['assigned_user_id'];
          } else {
            $errors['assignBlank'] = 'Не указан ответственный';
          }
          if (count($errors) === 0) {
            
            $isChangeAssign = $task->changeAssign($params['id'], $data);
            if ($isChangeAssign) {
              header('Location: ./?c=task&a=getList');
            }
          }
        }
      }
      Di::get()->render('task.twig', [
        'currentUser' => $mainList['currentUser'],
        'tasksForAuthor' => $mainList['tasksForAuthor'],
        'currentUser' => $mainList['currentUser'],
        'users' => $mainList['users'],
        'tasksForAssigned' => $mainList['tasksForAssigned'],
        'assignErrors' => $errors
      ]);
    }
    
  }

