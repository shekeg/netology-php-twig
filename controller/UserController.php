<?php
  include 'model/User.php';

  class UserController {

     public function login() {
      
      $user = new User();
      $errors = [];
      if (count($_POST) > 0) {
        if (empty($_POST['login'])) {
          $errors['loginBlank'] = 'Логин не указан';
        }
        if (empty($_POST['password'])) {
          $errors['passBlank'] = 'Пароль не указан';
        }
        if (count($errors) === 0) {
          if (!empty($_POST['sign_in'])) {
            $isSignIn = $user->signIn($_POST['login'], $_POST['password']);
            if (!$isSignIn) $errors['signIn'] = 'Ошибка входа';
          }
          if (!empty($_POST['sign_up'])) {
            $isSignUp = $user->signUp($_POST['login'], $_POST['password']);
            if (!$isSignUp) $errors['signUn'] = 'Ошибка регистрации';
          }
          if (!empty($isSignIn) or !empty($isSignUp))
          //if ($isSignIn or $isSignUp)
            header('Location: ./?c=task&a=getList');
        }
      }
      Di::get()->render('login.twig', ['errors' => $errors]);
    }

    public function logout() {
      session_destroy();
      header('Location: index.php');
    }

  }