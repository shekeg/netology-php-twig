<?php

  session_start();

if (! isset($_GET['c']) || ! isset($_GET['a'])) {
    $controller = 'user';
    $action = 'login';
} else {
    $controller = $_GET['c'];
    $action = $_GET['a'];
    if (!empty($_GET['id']))
      $params['id'] = $_GET['id'];
}

$controllerText = $controller . 'Controller';
$controllerFile = 'controller/' . ucfirst($controllerText) . '.php';
if (is_file($controllerFile)) {
  include $controllerFile;
  if (class_exists($controllerText)) {
      $controller = new $controllerText();
      if (method_exists($controller, $action)) {
          if (!empty($params)) {
            $controller->$action($params);  
          } else {
            $controller->$action();
          }
      }
  }
}