<?php
  class User {

    public static function checkAuth() {
      if (empty($_SESSION['user_id']) or empty($_SESSION['user_id']) or empty($_SESSION['password'])) {
        http_response_code(403);
        die();
      }
    }

    private static function setSalt($password) {
      $salt1 = 'neto';
      $salt2 = 'logy';
      return $password = $salt1 . $password . $salt2;
    }

    public function signIn($login, $password) {
      $sth = Di::get()->db()->prepare('SELECT id, login, password FROM `user` WHERE login=:login');
      $login = strip_tags($login);
      $sth->bindValue(':login', $login);
      $sth->execute();
      $user = $sth->fetch();
      if (empty($user)) return false;
      if (password_verify($user['password'], self::setSalt($password))) return false;
      $_SESSION['user_id'] = $user['id'];
      $_SESSION['login'] = $user['login'];
      $_SESSION['password'] = $user['password'];
      return true;
    }

    public function signUp($login, $password) {
      $sth = Di::get()->db()->prepare('SELECT login FROM user WHERE login=:login');
      $login = strip_tags($login);
      $sth->bindParam(':login', $login);
      $sth->execute();
      $user = $sth->fetch();
      if (!empty($user)) {
        return false;
      } else {
        $sth = Di::get()->db()->prepare('INSERT INTO user (login, password) VALUES (:login, :password)');
        $sth->bindParam(':login', $login);
        $password = strip_tags($password);
        $password = password_hash(self::setSalt($password), PASSWORD_DEFAULT);
        $sth->bindParam(':password', $password);
        $sth->execute();
        $_SESSION['user_id'] = Di::get()->db()->lastInsertId();
        $_SESSION['login'] = $login;
        $_SESSION['password'] = $password;
        return true;
      }
    }

    public function getCurrentUser() {
      $currentUser =  [
        'user_id' => $_SESSION['user_id'],
        'login' => $_SESSION['login'],
        'password' => $_SESSION['password']
      ];
      return $currentUser;
    }

    public function getList() {
      $sth = Di::get()->db()->prepare('SELECT id, login FROM user');
      if ($sth->execute()) {
        return $sth->fetchAll(PDO::FETCH_ASSOC);
      }
      return false;
    }

  }