<?php
  class Task {

    public function getListForAuthor($userID, $sortBy = '') {
      $query = 'SELECT t.id, t.description, t.is_done, t.date_added, t.user_id ,u.login user, au.login assigned_user 
        FROM task t JOIN user u ON t.user_id=u.id JOIN user au ON t.assigned_user_id=au.id WHERE t.user_id=:user_id';
      if ($sortBy)
        $query .= " ORDER BY {$sortBy}";
      $sth = DI::get()->db()->prepare($query);
      $userID = (int) $userID;
      $sth->bindParam('user_id', $userID);
      if ($sth->execute()) {
        return $sth->fetchAll(PDO::FETCH_ASSOC);
      }
      return false;
    }

    public function getListForAssigned($userID, $sortBy = '') {
      $query = 'SELECT t.id, t.description, t.is_done, t.date_added, t.user_id ,u.login user, au.login assigned_user 
        FROM task t JOIN user u ON t.user_id=u.id JOIN user au ON t.assigned_user_id=au.id 
        WHERE t.assigned_user_id=:assigned_user_id AND t.user_id!=:assigned_user_id';
      if ($sortBy)
        $query .= " ORDER BY {$sortBy}";
      $sth = DI::get()->db()->prepare($query);
      $userID = (int) $userID;
      $sth->bindParam('assigned_user_id', $userID);
      if ($sth->execute()) {
        return $sth->fetchAll(PDO::FETCH_ASSOC);
      }
      return false;
    }

    public function find($id) {
      $sth = DI::get()->db()->prepare('SELECT * FROM task WHERE id=:id');
      $id = (int) $id;
      $sth->bindParam(':id', $id);
      if ($sth->execute()) {
        return $sth->fetch();
      }
      return false;
    }

    public function add($data) {
      $sth = DI::get()->db()->prepare('INSERT INTO task (user_id, assigned_user_id, description, is_done, date_added)
        VALUES (:user_id, :assigned_user_id , :description, :is_done, :date_added)');
      $userID = (int) $data['userID'];
      $description = strip_tags($data['description']);
      $sth->bindParam(':user_id', $userID);
      $sth->bindParam(':assigned_user_id', $userID);
      $sth->bindParam(':description',$description);
      $sth->bindValue(':is_done', 0);
      $sth->bindValue(':date_added', date('Y-m-d h:m:s'));
      return $sth->execute();
    }

    public function save($id, $data) {
      $sth = DI::get()->db()->prepare('UPDATE task SET description=:description WHERE id=:id');
      $id = (int) strip_tags($id);
      $description = strip_tags($data['description']);
      $sth->bindParam(':id', $id);
      $sth->bindParam(':description', $description);
      return $sth->execute();
    }

    public function complete($id) {
      $sth = Di::get()->db()->prepare('UPDATE task SET is_done=:is_done WHERE id=:id');
      $id = (int) strip_tags($id);
      $sth->bindParam(':id', $id);
      $sth->bindValue(':is_done', 1);
      return $sth->execute();
    }

    public function delete($id) {
      $sth = Di::get()->db()->prepare('DELETE FROM task WHERE id=:id');
      $sth->bindValue(':id', $id, PDO::PARAM_INT);
      return $sth->execute();
    }

    public function changeAssign($id, $data) {
      $sth = Di::get()->db()->prepare('UPDATE task SET assigned_user_id=:assigned_user_id WHERE id=:id');
      $id = (int) strip_tags($id);
      $assignedUserID = (int) strip_tags($data['assignedUserID']);
      $sth->bindParam(':id', $id);
      $sth->bindParam(':assigned_user_id', $assignedUserID);
      return $sth->execute();
    }
  }