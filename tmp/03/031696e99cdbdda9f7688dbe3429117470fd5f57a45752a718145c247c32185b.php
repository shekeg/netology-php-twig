<?php

/* login.twig */
class __TwigTemplate_4b5944746ed42c48bde98aaa9d0964f0ea767a274f7c09ff22fc864a23c6f75c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<html lang=\"ru\">
<head>
  <meta charset=\"UTF-8\">
  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
  <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
  <title>Аутентификация</title>
</head>
<body>
  ";
        // line 9
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["errors"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
            // line 10
            echo "    <p>";
            echo twig_escape_filter($this->env, $context["error"], "html", null, true);
            echo "</p>
  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "  <form action=\"index.php\" method=\"POST\">
    <label for=\"login\">Логин</label>
    <input type=\"text\" name=\"login\" id=\"login\" autocomplete=\"off\">
    <label for=\"password\">Пароль</label>
    <input type=\"password\" name=\"password\" id=\"password\" autocomplete=\"off\">
    <input type=\"submit\" name=\"sign_in\" value=\"Войти\">
    <input type=\"submit\" name=\"sign_up\" value=\"Заригестрироваться\">
  </form>
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "login.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 12,  33 => 10,  29 => 9,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "login.twig", "C:\\xampp\\htdocs\\Projects\\netology\\php\\netology-php-twig\\template\\login.twig");
    }
}
