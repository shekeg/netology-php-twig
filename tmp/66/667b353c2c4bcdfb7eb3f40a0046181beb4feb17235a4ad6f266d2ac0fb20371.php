<?php

/* task.twig */
class __TwigTemplate_58bb3abe46bf5fb24ac826e28604d3661e2dffa15b20d231acab7e670af83090 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<html lang=\"ru\">
<head>
  <meta charset=\"UTF-8\">
  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
  <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
  <title>Задачи</title>
  <link rel=\"stylesheet\" href=\"main.css\">
</head>
<body>
  <div class=\"wrap\">

    <h1>Здравствуйте, ";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute(($context["currentUser"] ?? null), "login", array(), "array"), "html", null, true);
        echo "! </h1>
    ";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["addErrors"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["addError"]) {
            // line 14
            echo "      <p>";
            echo twig_escape_filter($this->env, $context["addError"], "html", null, true);
            echo "</p>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['addError'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 16
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["saveErrors"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["saveError"]) {
            // line 17
            echo "      <p>";
            echo twig_escape_filter($this->env, $context["saveError"], "html", null, true);
            echo "</p>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['saveError'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo "
    ";
        // line 20
        if (twig_test_empty(($context["taskForEdit"] ?? null))) {
            // line 21
            echo "      <form action=\"./?c=task&a=add\" method=\"POST\">
        <label for=\"description\">Описание</label>
        <input type=\"text\" name=\"description\" id=\"description\" autocomplete=\"off\">
        <input type=\"submit\" name=\"add\" value=\"Добавить\">
      </form>
    ";
        }
        // line 27
        echo "
    ";
        // line 28
        if (($context["taskForEdit"] ?? null)) {
            // line 29
            echo "      <form action=\"?c=task&a=save&id=";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["taskForEdit"] ?? null), "id", array(), "array"), "html", null, true);
            echo "\" method=\"POST\">
        <label for=\"description-edit\">Описание</label>
        <input type=\"text\" name=\"description\" id=\"description\" value=\"";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute(($context["taskForEdit"] ?? null), "description", array(), "array"), "html", null, true);
            echo "\" autocomplete=\"off\">
        <input type=\"submit\" name=\"save\" value=\"Сохранить\">
      </form>
    ";
        }
        // line 35
        echo "
    <form action=\"?c=task&a=sort\" method=\"POST\">
      <label for=\"sort-by\">Сортировка по: </label>
      <select name=\"sort_by\" id=\"sort-by\" >
        <option value=\"date_added\">Дате добавления</option>
        <option value=\"is_done\">Статусу</option>
        <option value=\"description\">Описанию</option>
      </select>
      <input type=\"submit\" name=\"sort\" value=\"Отсортировать\">
    </form>

    <a href=\"./?c=user&a=logout\">Выйти из системы</a>

    ";
        // line 48
        if ((twig_test_empty(($context["tasksForAuthor"] ?? null)) && twig_test_empty(($context["tasksForAssigned"] ?? null)))) {
            // line 49
            echo "      <p>Нет активных задач</p>
    ";
        }
        // line 51
        echo "
    ";
        // line 52
        if (($context["tasksForAuthor"] ?? null)) {
            // line 53
            echo "      <table class=\"table\">
        <thead>
          <tr>
            <th>Описание задачи</th>
            <th>Дата добавления</th>
            <th>Статус</th>
            <th>Действия</th>
            <th>Ответственный</th>
            <th>Автор</th>
            <th>Закрепить задачу за пользователем</th>
          </tr>
        </thead>
        ";
            // line 65
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["tasksForAuthor"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["task"]) {
                // line 66
                echo "          <tr>
            <td>";
                // line 67
                echo twig_escape_filter($this->env, $this->getAttribute($context["task"], "description", array(), "array"), "html", null, true);
                echo "</td>
            <td>";
                // line 68
                echo twig_escape_filter($this->env, $this->getAttribute($context["task"], "date_added", array(), "array"), "html", null, true);
                echo "</td>
            <td class=\"";
                // line 69
                echo (($this->getAttribute($context["task"], "is_done", array(), "array")) ? ("complete") : ("not-complete"));
                echo "\">";
                echo (($this->getAttribute($context["task"], "is_done", array(), "array")) ? ("Выполнено") : ("В процессе"));
                echo "</td>
            <td>
              <a href=\"?c=task&a=editPrepare&id=";
                // line 71
                echo twig_escape_filter($this->env, $this->getAttribute($context["task"], "id", array(), "array"), "html", null, true);
                echo "\">Изменить</a>
              <a href=\"?c=task&a=complete&id=";
                // line 72
                echo twig_escape_filter($this->env, $this->getAttribute($context["task"], "id", array(), "array"), "html", null, true);
                echo "\">Выполнить</a>
              <a href=\"?c=task&a=delete&id=";
                // line 73
                echo twig_escape_filter($this->env, $this->getAttribute($context["task"], "id", array(), "array"), "html", null, true);
                echo "\">Удалить</a>
            </td>
            <td>
              ";
                // line 76
                echo twig_escape_filter($this->env, $this->getAttribute($context["task"], "assigned_user", array(), "array"), "html", null, true);
                echo "
            </td>
            <td>
              ";
                // line 79
                echo twig_escape_filter($this->env, $this->getAttribute($context["task"], "user", array(), "array"), "html", null, true);
                echo "
            </td>
            <td>
              <form action=\"./?c=task&a=changeAssign&id=";
                // line 82
                echo twig_escape_filter($this->env, $this->getAttribute($context["task"], "id", array(), "array"), "html", null, true);
                echo "\" method=\"POST\">
                <select name=\"assigned_user_id\" id=\"assigned-user-id\">
                  ";
                // line 84
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["users"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
                    // line 85
                    echo "                    <option value=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "id", array(), "array"), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "login", array(), "array"), "html", null, true);
                    echo "</option>
                  ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 87
                echo "                </select>
                <input type=\"submit\" name=\"change_assign_user\" value=\"Переложить ответственность\">
              </form>
            </td>
          </tr>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['task'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 93
            echo "      </table>
    ";
        }
        // line 95
        echo "
    ";
        // line 96
        if (($context["tasksForAssigned"] ?? null)) {
            // line 97
            echo "      <h2>Также, посмотрите, что от Вас требуют другие люди:</h2>
      <table class=\"table\">
        <thead>
          <tr>
            <th>Описание задачи</th>
            <th>Дата добавления</th>
            <th>Статус</th>
            <th>Действия</th>
            <th>Ответственный</th>
            <th>Автор</th>
          </tr>
        </thead>
        ";
            // line 109
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["tasksForAssigned"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["task"]) {
                // line 110
                echo "          <tr>
            <td>";
                // line 111
                echo twig_escape_filter($this->env, $this->getAttribute($context["task"], "description", array(), "array"), "html", null, true);
                echo "</td>
            <td>";
                // line 112
                echo twig_escape_filter($this->env, $this->getAttribute($context["task"], "date_added", array(), "array"), "html", null, true);
                echo "</td>
            <td class=\"";
                // line 113
                echo (($this->getAttribute($context["task"], "is_done", array(), "array")) ? ("complete") : ("not-complete"));
                echo "\">";
                echo (($this->getAttribute($context["task"], "is_done", array(), "array")) ? ("Выполнено") : ("В процессе"));
                echo "</td>
            <td>
              <a href=\"?c=task&a=editPrepare&id=";
                // line 115
                echo twig_escape_filter($this->env, $this->getAttribute($context["task"], "id", array(), "array"), "html", null, true);
                echo "\">Изменить</a>
              <a href=\"?c=task&a=complete&id=";
                // line 116
                echo twig_escape_filter($this->env, $this->getAttribute($context["task"], "id", array(), "array"), "html", null, true);
                echo "\">Выполнить</a>
              <a href=\"?c=task&a=delete&id=";
                // line 117
                echo twig_escape_filter($this->env, $this->getAttribute($context["task"], "id", array(), "array"), "html", null, true);
                echo "\">Удалить</a>
            </td>
            <td>
              ";
                // line 120
                echo twig_escape_filter($this->env, $this->getAttribute($context["task"], "assigned_user", array(), "array"), "html", null, true);
                echo "
            </td>
            <td>
              ";
                // line 123
                echo twig_escape_filter($this->env, $this->getAttribute($context["task"], "user", array(), "array"), "html", null, true);
                echo "
            </td>
          </tr>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['task'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 127
            echo "      </table>    
    ";
        }
        // line 129
        echo "  </div>
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "task.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  292 => 129,  288 => 127,  278 => 123,  272 => 120,  266 => 117,  262 => 116,  258 => 115,  251 => 113,  247 => 112,  243 => 111,  240 => 110,  236 => 109,  222 => 97,  220 => 96,  217 => 95,  213 => 93,  202 => 87,  191 => 85,  187 => 84,  182 => 82,  176 => 79,  170 => 76,  164 => 73,  160 => 72,  156 => 71,  149 => 69,  145 => 68,  141 => 67,  138 => 66,  134 => 65,  120 => 53,  118 => 52,  115 => 51,  111 => 49,  109 => 48,  94 => 35,  87 => 31,  81 => 29,  79 => 28,  76 => 27,  68 => 21,  66 => 20,  63 => 19,  54 => 17,  49 => 16,  40 => 14,  36 => 13,  32 => 12,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "task.twig", "C:\\xampp\\htdocs\\Projects\\netology\\php\\netology-php-twig\\template\\task.twig");
    }
}
