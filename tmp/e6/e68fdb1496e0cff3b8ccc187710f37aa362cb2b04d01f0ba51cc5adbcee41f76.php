<?php

/* task.twig */
class __TwigTemplate_3ae1658255e9c7fdcbf7db0d77880adc19eca03ad97ef6994bd5a0f7f4b949b6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<html lang=\"ru\">
<head>
  <meta charset=\"UTF-8\">
  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
  <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
  <title>Задачи</title>
  <link rel=\"stylesheet\" href=\"main.css\">
</head>
<body>
  <div class=\"wrap\">

    <h1>Здравствуйте, ";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute(($context["currentUser"] ?? null), "login", array(), "array"), "html", null, true);
        echo "! </h1>
    ";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["addErrors"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["addError"]) {
            // line 14
            echo "      <p>";
            echo twig_escape_filter($this->env, $context["addError"], "html", null, true);
            echo "</p>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['addError'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 16
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["saveErrors"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["saveError"]) {
            // line 17
            echo "      <p>";
            echo twig_escape_filter($this->env, $context["saveError"], "html", null, true);
            echo "</p>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['saveError'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo "
    ";
        // line 20
        if (twig_test_empty(($context["taskForEdit"] ?? null))) {
            // line 21
            echo "      <form action=\"./?c=task&a=add\" method=\"POST\">
        <label for=\"description\">Описание</label>
        <input type=\"text\" name=\"description\" id=\"description\" autocomplete=\"off\">
        <input type=\"submit\" name=\"add\" value=\"Добавить\">
      </form>
    ";
        }
        // line 27
        echo "
    ";
        // line 28
        if (($context["taskForEdit"] ?? null)) {
            // line 29
            echo "      <form action=\"?c=task&a=save&id=";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["taskForEdit"] ?? null), "id", array(), "array"), "html", null, true);
            echo "\" method=\"POST\">
        <label for=\"description-edit\">Описание</label>
        <input type=\"text\" name=\"description\" id=\"description\" value=\"";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute(($context["taskForEdit"] ?? null), "description", array(), "array"), "html", null, true);
            echo "\" autocomplete=\"off\">
        <input type=\"submit\" name=\"save\" value=\"Сохранить\">
      </form>
    ";
        }
        // line 35
        echo "
    <a href=\"./?c=user&a=logout\">Выйти из системы</a>



    ";
        // line 41
        echo "      <table class=\"table\">
        <thead>
          <tr>
            <th>Описание задачи</th>
            <th>Дата добавления</th>
            <th>Статус</th>
            <th>Действия</th>
            <th>Ответственный</th>
            <th>Автор</th>
            <th>Закрепить задачу за пользователем</th>
          </tr>
        </thead>
        ";
        // line 53
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["tasksForAuthor"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["task"]) {
            // line 54
            echo "          <tr>
            <td>";
            // line 55
            echo twig_escape_filter($this->env, $this->getAttribute($context["task"], "description", array(), "array"), "html", null, true);
            echo "</td>
            <td>";
            // line 56
            echo twig_escape_filter($this->env, $this->getAttribute($context["task"], "date_added", array(), "array"), "html", null, true);
            echo "</td>
            <td class=\"";
            // line 57
            echo (($this->getAttribute($context["task"], "is_done", array(), "array")) ? ("complete") : ("not-complete"));
            echo "\">";
            echo (($this->getAttribute($context["task"], "is_done", array(), "array")) ? ("Выполнено") : ("В процессе"));
            echo "</td>
            <td>
              <a href=\"?c=task&a=editPrepare&id=";
            // line 59
            echo twig_escape_filter($this->env, $this->getAttribute($context["task"], "id", array(), "array"), "html", null, true);
            echo "\">Изменить</a>
              <a href=\"?c=task&a=complete&id=";
            // line 60
            echo twig_escape_filter($this->env, $this->getAttribute($context["task"], "id", array(), "array"), "html", null, true);
            echo "\">Выполнить</a>
              <a href=\"?c=task&a=delete&id=";
            // line 61
            echo twig_escape_filter($this->env, $this->getAttribute($context["task"], "id", array(), "array"), "html", null, true);
            echo "\">Удалить</a>
            </td>
            <td>
              ";
            // line 64
            echo twig_escape_filter($this->env, $this->getAttribute($context["task"], "assigned_user", array(), "array"), "html", null, true);
            echo "
            </td>
            <td>
              ";
            // line 67
            echo twig_escape_filter($this->env, $this->getAttribute($context["task"], "user", array(), "array"), "html", null, true);
            echo "
            </td>
            <td>
              <form action=\"./?c=task&a=changeAssign&id=";
            // line 70
            echo twig_escape_filter($this->env, $this->getAttribute($context["task"], "id", array(), "array"), "html", null, true);
            echo "\" method=\"POST\">
                <select name=\"assigned_user_id\" id=\"assigned-user-id\">
                  ";
            // line 72
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["users"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
                // line 73
                echo "                    <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "id", array(), "array"), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "login", array(), "array"), "html", null, true);
                echo "</option>
                  ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 75
            echo "                </select>
                <input type=\"submit\" name=\"change_assign_user\" value=\"Переложить ответственность\">
              </form>
            </td>
          </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['task'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 81
        echo "      </table>
    ";
        // line 83
        echo "
    ";
        // line 84
        if (($context["tasksForAssigned"] ?? null)) {
            // line 85
            echo "      <h2>Также, посмотрите, что от Вас требуют другие люди:</h2>
      <table class=\"table\">
        <thead>
          <tr>
            <th>Описание задачи</th>
            <th>Дата добавления</th>
            <th>Статус</th>
            <th>Действия</th>
            <th>Ответственный</th>
            <th>Автор</th>
          </tr>
        </thead>
        ";
            // line 97
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["tasksForAssigned"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["task"]) {
                // line 98
                echo "          <tr>
            <td>";
                // line 99
                echo twig_escape_filter($this->env, $this->getAttribute($context["task"], "description", array(), "array"), "html", null, true);
                echo "</td>
            <td>";
                // line 100
                echo twig_escape_filter($this->env, $this->getAttribute($context["task"], "date_added", array(), "array"), "html", null, true);
                echo "</td>
            <td class=\"";
                // line 101
                echo (($this->getAttribute($context["task"], "is_done", array(), "array")) ? ("complete") : ("not-complete"));
                echo "\">";
                echo (($this->getAttribute($context["task"], "is_done", array(), "array")) ? ("Выполнено") : ("В процессе"));
                echo "</td>
            <td>
              <a href=\"?c=task&a=editPrepare&id=";
                // line 103
                echo twig_escape_filter($this->env, $this->getAttribute($context["task"], "id", array(), "array"), "html", null, true);
                echo "\">Изменить</a>
              <a href=\"?c=task&a=complete&id=";
                // line 104
                echo twig_escape_filter($this->env, $this->getAttribute($context["task"], "id", array(), "array"), "html", null, true);
                echo "\">Выполнить</a>
              <a href=\"?c=task&a=delete&id=";
                // line 105
                echo twig_escape_filter($this->env, $this->getAttribute($context["task"], "id", array(), "array"), "html", null, true);
                echo "\">Удалить</a>
            </td>
            <td>
              ";
                // line 108
                echo twig_escape_filter($this->env, $this->getAttribute($context["task"], "assigned_user", array(), "array"), "html", null, true);
                echo "
            </td>
            <td>
              ";
                // line 111
                echo twig_escape_filter($this->env, $this->getAttribute($context["task"], "user", array(), "array"), "html", null, true);
                echo "
            </td>
          </tr>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['task'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 115
            echo "      </table>    
    ";
        }
        // line 117
        echo "  </div>
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "task.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  272 => 117,  268 => 115,  258 => 111,  252 => 108,  246 => 105,  242 => 104,  238 => 103,  231 => 101,  227 => 100,  223 => 99,  220 => 98,  216 => 97,  202 => 85,  200 => 84,  197 => 83,  194 => 81,  183 => 75,  172 => 73,  168 => 72,  163 => 70,  157 => 67,  151 => 64,  145 => 61,  141 => 60,  137 => 59,  130 => 57,  126 => 56,  122 => 55,  119 => 54,  115 => 53,  101 => 41,  94 => 35,  87 => 31,  81 => 29,  79 => 28,  76 => 27,  68 => 21,  66 => 20,  63 => 19,  54 => 17,  49 => 16,  40 => 14,  36 => 13,  32 => 12,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "task.twig", "C:\\xampp\\htdocs\\Projects\\netology\\php\\netology-php-twig\\templ\\task.twig");
    }
}
