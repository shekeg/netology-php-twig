<?php

/* index.twig */
class __TwigTemplate_05cfd8e879676200bed0d6b68275e1af332aa62d6776dae6a677ab42d3243e19 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<html lang=\"ru\">
<head>
  <meta charset=\"UTF-8\">
  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
  <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
  <title>Document</title>
</head>
<body>
  <h1>Здравствуйте, ";
        // line 9
        echo twig_escape_filter($this->env, $this->getAttribute(($context["currentUser"] ?? null), "login", array(), "array"), "html", null, true);
        echo " </h1>
  ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["addErrors"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["addError"]) {
            // line 11
            echo "    <p>";
            echo twig_escape_filter($this->env, $context["addError"], "html", null, true);
            echo "</p>
  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['addError'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "  ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["saveErrors"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["saveError"]) {
            // line 14
            echo "    <p>";
            echo twig_escape_filter($this->env, $context["saveError"], "html", null, true);
            echo "</p>
  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['saveError'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 16
        echo "
  ";
        // line 17
        if (twig_test_empty(($context["taskForEdit"] ?? null))) {
            // line 18
            echo "    <form action=\"./?c=task&a=add\" method=\"POST\">
      <label for=\"description\">Описание</label>
      <input type=\"text\" name=\"description\" id=\"description\" autocomplete=\"off\">
      <input type=\"submit\" name=\"add\" value=\"Добавить\">
    </form>
  ";
        }
        // line 24
        echo "
  ";
        // line 25
        if (($context["taskForEdit"] ?? null)) {
            // line 26
            echo "    <form action=\"?c=task&a=save&id=<?= \$taskForEdit['id']?>\" method=\"POST\">
      <label for=\"description-edit\">Описание</label>
      <input type=\"text\" name=\"description\" id=\"description\" value=\"<?php echo \$taskForEdit['description']?>\" autocomplete=\"off\">
      <input type=\"submit\" name=\"save\" value=\"Сохранить\">
    </form>
  ";
        }
        // line 32
        echo "
  <a href=\"./?c=user&a=logout\">Выйти из системы</a>

  ";
        // line 35
        if (($context["tasksForAuthor"] ?? null)) {
            // line 36
            echo "    <table class=\"table\">
      <thead>
        <tr>
          <th>Описание задачи</th>
          <th>Дата добавления</th>
          <th>Статус</th>
          <th>Действия</th>
          <th>Ответственный</th>
          <th>Автор</th>
          <th>Закрепить задачу за пользователем</th>
        </tr>
      </thead>
      ";
            // line 48
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["tasksForAuthor"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["task"]) {
                // line 49
                echo "        <tr>
          <td>";
                // line 50
                echo twig_escape_filter($this->env, $this->getAttribute($context["task"], "description", array(), "array"), "html", null, true);
                echo "</td>
          <td>";
                // line 51
                echo twig_escape_filter($this->env, $this->getAttribute($context["task"], "date_added", array(), "array"), "html", null, true);
                echo "</td>
          <td class=\"";
                // line 52
                echo (($this->getAttribute($context["task"], "is_done", array(), "array")) ? ("complete") : ("not-complete"));
                echo "\">";
                echo (($this->getAttribute($context["task"], "is_done", array(), "array")) ? ("Выполнено") : ("В процессе"));
                echo "</td>
          <td>
            <a href=\"?c=task&a=editPrepare&id=";
                // line 54
                echo twig_escape_filter($this->env, $this->getAttribute($context["task"], "id", array(), "array"), "html", null, true);
                echo "\">Изменить</a>
            <a href=\"?c=task&a=complete&id=";
                // line 55
                echo twig_escape_filter($this->env, $this->getAttribute($context["task"], "id", array(), "array"), "html", null, true);
                echo "\">Выполнить</a>
            <a href=\"?c=task&a=delete&id=";
                // line 56
                echo twig_escape_filter($this->env, $this->getAttribute($context["task"], "id", array(), "array"), "html", null, true);
                echo "\">Удалить</a>
          </td>
          <td>
            ";
                // line 59
                echo twig_escape_filter($this->env, $this->getAttribute($context["task"], "assigned_user", array(), "array"), "html", null, true);
                echo "
          </td>
          <td>
            ";
                // line 62
                echo twig_escape_filter($this->env, $this->getAttribute($context["task"], "user", array(), "array"), "html", null, true);
                echo "
          </td>
          <td>
            <form action=\"./?c=task&a=changeAssign&id=";
                // line 65
                echo twig_escape_filter($this->env, $this->getAttribute($context["task"], "id", array(), "array"), "html", null, true);
                echo "\" method=\"POST\">
              <select name=\"assigned_user_id\" id=\"assigned-user-id\">
                ";
                // line 67
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["users"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
                    // line 68
                    echo "                  <option value=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "id", array(), "array"), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "login", array(), "array"), "html", null, true);
                    echo "</option>
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 70
                echo "              </select>
              <input type=\"submit\" name=\"change_assign_user\" value=\"Переложить ответственность\">
            </form>
          </td>
        </tr>
      ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['task'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 76
            echo "    </table>
  ";
        }
        // line 78
        echo "
  ";
        // line 79
        if (($context["tasksForAssigned"] ?? null)) {
            // line 80
            echo "    <h2>Также, посмотрите, что от Вас требуют другие люди:</h2>
    <table class=\"table\">
      <thead>
        <tr>
          <th>Описание задачи</th>
          <th>Дата добавления</th>
          <th>Статус</th>
          <th>Действия</th>
          <th>Ответственный</th>
          <th>Автор</th>
        </tr>
      </thead>
      ";
            // line 92
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["tasksForAssigned"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["task"]) {
                // line 93
                echo "        <tr>
          <td>";
                // line 94
                echo twig_escape_filter($this->env, $this->getAttribute($context["task"], "description", array(), "array"), "html", null, true);
                echo "</td>
          <td>";
                // line 95
                echo twig_escape_filter($this->env, $this->getAttribute($context["task"], "date_added", array(), "array"), "html", null, true);
                echo "</td>
          <td class=\"";
                // line 96
                echo (($this->getAttribute($context["task"], "is_done", array(), "array")) ? ("complete") : ("not-complete"));
                echo "\">";
                echo (($this->getAttribute($context["task"], "is_done", array(), "array")) ? ("Выполнено") : ("В процессе"));
                echo "</td>
          <td>
            <a href=\"?c=task&a=editPrepare&id=";
                // line 98
                echo twig_escape_filter($this->env, $this->getAttribute($context["task"], "id", array(), "array"), "html", null, true);
                echo "\">Изменить</a>
            <a href=\"?c=task&a=complete&id=";
                // line 99
                echo twig_escape_filter($this->env, $this->getAttribute($context["task"], "id", array(), "array"), "html", null, true);
                echo "\">Выполнить</a>
            <a href=\"?c=task&a=delete&id=";
                // line 100
                echo twig_escape_filter($this->env, $this->getAttribute($context["task"], "id", array(), "array"), "html", null, true);
                echo "\">Удалить</a>
          </td>
          <td>
            ";
                // line 103
                echo twig_escape_filter($this->env, $this->getAttribute($context["task"], "assigned_user", array(), "array"), "html", null, true);
                echo "
          </td>
          <td>
            ";
                // line 106
                echo twig_escape_filter($this->env, $this->getAttribute($context["task"], "user", array(), "array"), "html", null, true);
                echo "
          </td>
        </tr>
      ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['task'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 110
            echo "    </table>    
  ";
        }
        // line 112
        echo "</body>
</html>";
    }

    public function getTemplateName()
    {
        return "index.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  265 => 112,  261 => 110,  251 => 106,  245 => 103,  239 => 100,  235 => 99,  231 => 98,  224 => 96,  220 => 95,  216 => 94,  213 => 93,  209 => 92,  195 => 80,  193 => 79,  190 => 78,  186 => 76,  175 => 70,  164 => 68,  160 => 67,  155 => 65,  149 => 62,  143 => 59,  137 => 56,  133 => 55,  129 => 54,  122 => 52,  118 => 51,  114 => 50,  111 => 49,  107 => 48,  93 => 36,  91 => 35,  86 => 32,  78 => 26,  76 => 25,  73 => 24,  65 => 18,  63 => 17,  60 => 16,  51 => 14,  46 => 13,  37 => 11,  33 => 10,  29 => 9,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "index.twig", "C:\\xampp\\htdocs\\Projects\\netology\\php\\netology-php-twig\\templ\\index.twig");
    }
}
